from colorama import Fore, init, Style
import argparse

init(autoreset=True)

def process_payment_string(txt):
    payer = txt.split("paid")[0].strip().lower()
    amount = txt.split("paid")[1].split("at")[0]
    place = txt.split("at")[1].split("for")[0]
    beneficiaries = [beneficiary.strip() for beneficiary in txt.split("for")[1].split(",")]

    return payer, amount, place, beneficiaries

def settle_balances(balances):
    transactions = []

    creditors = {person: balance for person, balance in balances.items() if balance > 0}
    debtors = {person: balance for person, balance in balances.items() if balance < 0}

    for debtor, debtor_balance in debtors.items():
        for creditor, creditor_balance in creditors.items():
            if balances[creditor] == 0:
                continue
            settle_amount = min(-balances[debtor], balances[creditor])
            transactions.append((debtor, creditor, settle_amount))

            balances[debtor] += settle_amount
            balances[creditor] -= settle_amount

            if round(balances[debtor]) == 0:
                break

    return transactions


def main():
    parser = argparse.ArgumentParser(description="Split payments to a variable number of actors")
    parser.add_argument("filename", help="text file where the transactions are stored")
    parser.add_argument("--showbalances", "-b", help="show balances after each transaction")
    args = parser.parse_args();

    balances = {}
    totalSpent = 0
    with open(args.filename, 'r') as p:
        for payment_string in p:
            if payment_string.strip() == "" or payment_string.lstrip()[0] == '#':
                continue
            payer, amount, place, benficiaries = process_payment_string(payment_string)
            totalSpent = totalSpent + int(amount)

            print(f"* {Style.BRIGHT}{payment_string}", end='')

            # print("Payer:", payer)
            # print("Amount:", amount)
            # print("Place:", place)
            # print("Benficiaries:", end=" ")
            # for beneficiary in benficiaries:
            #     print(f"{beneficiary}, ", end="")
            # print("\n")


            balances[payer] = balances.get(payer, 0) + round(float(amount))
            for beneficiary in benficiaries:
                balances[beneficiary] = balances.get(beneficiary, 0) - round(float(amount)/len(benficiaries))

            if args.showbalances:
                print(f"Balances after this transaction...")
                for k, v in balances.items():
                    print(f"{k}: {Fore.GREEN if v < 0 else Fore.RED} {v}")
                print()
    tranx = settle_balances(balances)
    print()
    print("Settlement transactions:")
    print("========================")
    for transaction in tranx:
        print(f"{transaction[0]} "
               f"pays {transaction[1]} {transaction[2]:.2f}")

    printed = False
    for k, v in balances.items():
        if v != 0:
            if not printed:
                print(f"\nResidue:")
                print(f"========")
                printed = True
            print(f"{k}: {v}")

    print()
    print("Fun facts...")
    count = len(balances.items())
    print(f"* The group spent a total of {Fore.RED}{Style.BRIGHT}{totalSpent}", end="")
    print(f" among {Style.BRIGHT}{count} people")
    print(f"* On an average, each person spent {Style.BRIGHT}{totalSpent/len(balances.items()):.2f}")
    joys = [transaction[2] for transaction in tranx]
    joyDiscrepancyRatio = max(joys) / min(joys)
    sty = Fore.GREEN
    if joyDiscrepancyRatio > 10.0:
        sty = Fore.RED
    print(f"* This joy discrepancy ratio was {sty}{Style.BRIGHT}{joyDiscrepancyRatio:.2f}")






if __name__ == "__main__":
    main()
