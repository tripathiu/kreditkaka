import spacy
import sys



class TextProcess:
    def __init__(self):
        self.nlp = spacy.load("en_core_web_trf")  # import transformer model
        self.doc = None
        self.lineType : Literal["unknown","pay"] = "Unknown"
        self.users = ["ishaan", "udit", "divyanshi", "utkarsh"]
        self.root = None
        
    @staticmethod
    def findConjugates(token):
        conjugates = [token]
        for child in token.children:
            if child.dep_ == "conj":
                conjugates.extend(TextProcess.findConjugates(child))
        return conjugates

    def process(self, text):
        self.doc = self.nlp(text)

        self.root = None
        payLemmas = ["pay", "spend", "transfer", "give"]
        try:
            self.root = [sent.root for sent in self.doc.sents][0]
        except Exception as e:
            print(e)
        if self.root.lemma_ in payLemmas:
            self.lineType = "pay"
        print(f"{self.root = }, {self.lineType = }")

        if self.lineType == "pay":
            return self.processPayText()

    def processPayText(self):
        # identify the payer
        payer = None
        dependents = [token for token in self.doc if token.head == self.root]
        for dep in dependents:
            if dep.text in self.users and dep.dep_ == "nsubj":
                payer = dep
                break

        # amount paid
        amount = None
        for dep in dependents:
            if dep.like_num:
                amount = float(dep.text)
                break
        if amount is None:
            for token in self.doc:
                if token.like_num:
                    amount = float(token.text)
                    break

        # payees
        payees = []
        for dep in dependents:
            if payees != []:
                break
            elif dep == payer:
                continue
            elif dep.dep_ == "ROOT":
                continue
            elif dep.dep_ == "prep":
                prepdeps =  [token for token in self.doc if token.head == dep]
                for prepdep in prepdeps:
                    if prepdep.dep_ in ["pobj", "dobj", "dative"]:
                        payees = TextProcess.findConjugates(prepdep)
            elif dep.like_num:
                continue
            elif dep.dep_ in ["dobj", "dative"]:
                payees = TextProcess.findConjugates(dep)
                continue

        data = {
            "payer": payer,
            "payees": payees,
            "amount": amount,
            "context": None
        }
        return data
                

def process(text):

    # identify the root
    payer = None
    amount = None



    


if __name__ == "__main__":
    for line in sys.stdin:
        textProcess = TextProcess()
        print(textProcess.process(line))
